# MiddleFork Base Image

## Docker skeleton for building the Middlefork base image.

### Just a Dockerfile, a docker-compose.yml and a README.

Mothership from which all are derived: Debian Stable, with NPM, and both Python 2 & 3 each in their own Virtualenvs.

## Build 
`docker-compose build`

## Or via DockerHub
` docker pull middlefork/baseimage`