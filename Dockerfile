FROM debian:stable

# Install our packages, including the special things we need to do to install node/npm.  And jove emacs replacement costs 600K, so why not?
RUN apt update \
 && apt install -y curl gnupg\
 && curl -sL https://deb.nodesource.com/setup_10.x |bash - \
 && apt install -y \
 autoconf \
 build-essential \
 jove \
 libtool \
 nodejs \
 python-pip \
 python3 \
 python3-pip \
 python3-venv \
 systemd \
 virtualenv 

# Setup virtual environments for both python 2 and python 3
RUN mkdir /opt/venv2 \
  && mkdir /opt/venv3 \
  && virtualenv /opt/venv2 \
  && python3 -m venv /opt/venv3 

# Choose the -default- python version on the fly or from VENV
#  --todo--

# Default CMD is bash, whoopie.
CMD bash

